enum type_e {
#define VMODENUM(x) type_ ## x,
	_TYPE_E_INVALID,
#include "tbl_enum_type.h"
	_TYPE_E_MAX
};
