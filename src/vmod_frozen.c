/*-
 * Copyright 2018 UPLEX - Nils Goroll Systemoptimierung
 * All rights reserved
 *
 * Author: Nils Goroll <nils.goroll@uplex.de>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL AUTHOR OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 */

#include "config.h"

#include <stdlib.h>
#include <string.h>
#include <cache/cache.h>
#include <vcl.h>

#include "vcc_frozen_if.h"
#include "vmod_frozen.h"
#include "frozen/frozen.h"

static void
errmsg(VRT_CTX, const char *fmt, ...)
{
	va_list args;

	AZ(ctx->method & VCL_MET_TASK_H);
	va_start(args, fmt);
	if (ctx->vsl)
		VSLbv(ctx->vsl, SLT_VCL_Error, fmt, args);
	else
		VSLv(SLT_VCL_Error, NO_VXID, fmt, args);
	va_end(args);
}

static enum type_e
type_parse(VCL_ENUM e) {
#define VMODENUM(n) if (e == VENUM(n)) return(type_ ## n);
#include "tbl_enum_type.h"
	WRONG("illegal type enum");
}

static const char * const type_s[_TYPE_E_MAX] = {
	[_TYPE_E_INVALID] = NULL,
#define VMODENUM(n) [type_ ## n] = #n,
#include "tbl_enum_type.h"
};

/*
 * for the vmod object, this struct is the specification
 *
 * .parse memcopies to array of expects and fills out the bottom
 */
struct vmod_frozen_expect {
	unsigned	magic;
#define VMOD_FROZEN_EXPECT_MAGIC	0x7d6dcdeb
	unsigned int	nullok:1;
	unsigned int	required:1;
	unsigned int	valid:1;	// written by .parse
	unsigned int	null:1;	// written by .parse
	const char	*path;

	// written by .parse
	const char	*val;
	int		len;
	// expected type initially, then overwritten
	enum type_e	type;
};

struct vmod_frozen_parser {
	unsigned			magic;
#define VMOD_FROZEN_PARSER_MAGIC	0x3c438bd9

	unsigned			limit;
	char				*vcl_name;
	unsigned			n_expect;
	unsigned			s_expect;
	struct vmod_frozen_expect	*expects;
};

struct vmod_frozen_task {
	unsigned			magic;
#define VMOD_FROZEN_TASK_MAGIC	0x5028fbb3
	unsigned			n_expect;
	unsigned			n_seen;
	unsigned			first;
	struct vmod_frozen_expect	*vals;
};

VCL_VOID
vmod_parser__init(VRT_CTX,
    struct vmod_frozen_parser **vfpap, const char *vcl_name, VCL_INT limit)
{
	struct vmod_frozen_parser *vfpa;
	struct vmod_frozen_expect *vfex;
	const unsigned min_s_expect = 64 / sizeof(*vfpa);

	AN(vfpap);
	AZ(*vfpap);

	AN(min_s_expect);

	if (limit < 1) {
		VRT_fail(ctx, "frozen.parser limit must be 1 or higher");
		return;
	}

	ALLOC_OBJ(vfpa, VMOD_FROZEN_PARSER_MAGIC);
	if (vfpa == NULL) {
		VRT_fail(ctx, "frozen.parser obj alloc failed");
		return;
	}

	REPLACE(vfpa->vcl_name, vcl_name);
	if (vfpa->vcl_name == NULL) {
		VRT_fail(ctx, "frozen.parser dup vcl_name failed");
		goto err_dup;
	}

	vfex = malloc(sizeof(*vfex) * min_s_expect);
	if (vfex == NULL) {
		VRT_fail(ctx, "frozen.parser alloc expects failed");
		goto err_exp;
	}

	vfpa->limit = limit;
	vfpa->s_expect = min_s_expect;
	vfpa->expects = vfex;

	*vfpap = vfpa;

	return;

  err_exp:
	free(vfpa->vcl_name);
  err_dup:
	FREE_OBJ(vfpa);
}

VCL_VOID
vmod_parser__fini(struct vmod_frozen_parser **vfpap)
{
	struct vmod_frozen_parser *vfpa = *vfpap;
	struct vmod_frozen_expect *vfex;
	unsigned i;

	*vfpap = NULL;
	if (vfpa == NULL)
		return;

	CHECK_OBJ(vfpa, VMOD_FROZEN_PARSER_MAGIC);

	for (i = 0; i < vfpa->n_expect; i++) {
		vfex = &vfpa->expects[i];
		free(TRUST_ME(vfex->path));
	}

	free(vfpa->expects);
	free(vfpa->vcl_name);
	FREE_OBJ(vfpa);
}

VCL_VOID
vmod_parser_expect(VRT_CTX,
    struct vmod_frozen_parser *vfpa, VCL_STRING path, VCL_ENUM type_arg,
    VCL_BOOL null, VCL_BOOL required)
{
	struct vmod_frozen_expect *vfex;
	enum type_e type = type_parse(type_arg);
	struct vmod_priv *task;
	unsigned n;

	CHECK_OBJ_NOTNULL(ctx, VRT_CTX_MAGIC);
	CHECK_OBJ_NOTNULL(vfpa, VMOD_FROZEN_PARSER_MAGIC);

	assert(ctx->method == VCL_MET_INIT);

	if (vfpa->n_expect == vfpa->s_expect) {
		n = vfpa->s_expect << 1;
		vfex = realloc(vfpa->expects, n * sizeof(*vfex));
		if (vfex == NULL) {
			VRT_fail(ctx, "%s.expect() realloc %d elements failed",
			    vfpa->vcl_name, n);
			return;
		}
		vfpa->expects = vfex;
		vfpa->s_expect = n;
	}

	/* invalidate parse task in case expect is called after parse */
	task = VRT_priv_task(ctx, vfpa);
	if (task != NULL)
		task->priv = NULL;

	assert(vfpa->s_expect > vfpa->n_expect);
	vfex = &vfpa->expects[vfpa->n_expect++];

	INIT_OBJ(vfex, VMOD_FROZEN_EXPECT_MAGIC);
	vfex->path = strdup(path);
	vfex->type = type;
	vfex->nullok = !!null;
	vfex->required = !!required;
}

static void vfex_cb(void *, const char *, size_t, const char *,
    const struct json_token *);

static VCL_BOOL
parse(VRT_CTX, struct vmod_frozen_parser *vfpaa,
    const char *s, size_t len)
{
	const struct vmod_frozen_parser *vfpa = vfpaa;
	struct vmod_frozen_task *vfta;
	struct vmod_frozen_expect *vfex;
	struct vmod_priv *task;
	struct frozen_args args[1];
	VCL_BOOL r;
	unsigned i;

	CHECK_OBJ_NOTNULL(ctx, VRT_CTX_MAGIC);
	CHECK_OBJ_NOTNULL(vfpa, VMOD_FROZEN_PARSER_MAGIC);

	INIT_FROZEN_ARGS(args);
	args->limit = vfpa->limit;

	if (s == NULL) {
		return (0);
	}

	if (vfpa->n_expect == 0)
		return (!! json_walk_args(s, strlen(s), args));

	assert(vfpa->n_expect > 0);

	task = VRT_priv_task(ctx, vfpa);

	if (task == NULL) {
		VRT_fail(ctx, "no priv_task");
		return (0);
	}

	if (task->priv) {
		CAST_OBJ_NOTNULL(vfta, task->priv,
		    VMOD_FROZEN_TASK_MAGIC);
		vfex = vfta->vals;
		INIT_OBJ(vfta, VMOD_FROZEN_TASK_MAGIC);
	} else {
		vfta = WS_Alloc(ctx->ws, sizeof *vfta);
		if (vfta == NULL) {
			VRT_fail(ctx, "%s.parse() WS_Alloc task failed",
			    vfpa->vcl_name);
			return (0);
		}
		INIT_OBJ(vfta, VMOD_FROZEN_TASK_MAGIC);

		vfex = WS_Alloc(ctx->ws, vfpa->n_expect * sizeof *vfex);
		if (vfex == NULL) {
			VRT_fail(ctx, "%s.parse() WS_Alloc task failed",
			    vfpa->vcl_name);
			return (0);
		}
		task->priv = vfta;
	}
	AN(vfex);
	vfta->n_expect = vfpa->n_expect;
	vfta->vals = vfex;
	memcpy(vfex, vfpa->expects, vfpa->n_expect * sizeof *vfex);

	args->callback = vfex_cb;
	args->callback_data = vfta;

	r = !! json_walk_args(s, len, args);

	// prep for extract
	vfta->first = 0;

	if (r == 0)
		return (r);
	if (vfta->n_expect == vfta->n_seen)
		return (r);
	for (i = 0; i < vfta->n_expect; i++) {
		vfex = &vfta->vals[i];
		if (vfex->required == 1 &&
		    vfex->valid == 0)
			return (0);
	}
	return(r);
}

VCL_BOOL
vmod_parser_parse(VRT_CTX, struct vmod_frozen_parser *vfpaa,
    const char *s)
{
	return (parse(ctx, vfpaa, s, s != NULL ? strlen(s) : 0));
}

struct collect_iter_priv {
	unsigned			magic;
#define COLLECT_ITER_PRIV_MAGIC 0x6ddd62cd
	char				*heap;	// only if realloc'ed
	const char			*ptr;
	size_t				len;
	size_t				max;
};

static int v_matchproto_(objiterate_f)
collect_iter_f(void *priv, unsigned flush, const void *ptr, ssize_t len)
{
	struct collect_iter_priv *cip;
	char *heap;
	size_t l;

	CAST_OBJ_NOTNULL(cip, priv, COLLECT_ITER_PRIV_MAGIC);

	if (ptr == NULL || len == 0)
		return (0);

	/*
	 * if this is the only segment, return without copying.
	 *
	 * comparing flush equal is deliberate: This is not to be true for
	 * OBJ_ITER_FLUSH
	 */

	if (cip->ptr == NULL && flush == OBJ_ITER_END) {
		AZ(cip->heap);
		AZ(cip->len);
		cip->ptr = ptr;
		cip->len = len;
		return (0);
	}

	l = cip->len + len;
	if (l > cip->max) {
		free(cip->heap);
		cip->heap = NULL;
		cip->ptr = NULL;
		cip->len = l;
		return (1);
	}

	heap = realloc(cip->heap, cip->len + len);
	AN(heap);
	memcpy(heap + cip->len, ptr, len);
	cip->heap = heap;
	cip->ptr = heap;
	cip->len += len;
	return (0);
}

/* we need to keep the body copy around until the end of the task
 * because .extract uses references to it after .parse_body().
 *
 * to hand the heap ptr to a priv_task if we have one
 */

static void
collect_free(VRT_CTX, void *p) {
	(void) ctx;
	free(p);
}

struct vmod_priv_methods collect_priv_methods[1] = {{
	.magic = VMOD_PRIV_METHODS_MAGIC,
	.type = "frozen collect delayed free",
	.fini = collect_free
}};

VCL_BOOL
vmod_parser_parse_body(VRT_CTX,
    struct VPFX(frozen_parser) *vfpaa, VCL_ENUM which, VCL_BYTES max)
{
	struct collect_iter_priv cip[1];
	struct vmod_priv *priv_task;
	VCL_BOOL ret;
	int r;

	CHECK_OBJ_NOTNULL(ctx, VRT_CTX_MAGIC);

	INIT_OBJ(cip, COLLECT_ITER_PRIV_MAGIC);
	cip->max = max;

	if (which == VENUM(req_body)) {
		if (ctx->req == NULL) {
			errmsg(ctx, "xfrozen.parse_body(which = req_body) "
			    "called but no request body found");
			return (0);
		}
		else {
			r = VRB_Iterate(ctx->req->wrk, ctx->vsl, ctx->req,
			    collect_iter_f, cip);
		}
	}
	else if (which == VENUM(bereq_body) &&
	     ctx->bo != NULL && ctx->bo->bereq_body != NULL) {
		r = ObjIterate(ctx->bo->wrk, ctx->bo->bereq_body,
		    cip, collect_iter_f, 0);
	}
	else if (which == VENUM(bereq_body)) {
		if (ctx->bo == NULL || ctx->bo->req == NULL) {
			errmsg(ctx, "xfrozen.parse_body(which = bereq_body) "
			    "called but no backend request body found");
			return (0);
		}
		else {
			r = VRB_Iterate(ctx->bo->wrk, ctx->vsl, ctx->bo->req,
			    collect_iter_f, cip);
		}
	}
	else if (which == VENUM(resp_body)) {
		if (ctx->req == NULL || ctx->req->objcore == NULL) {
			errmsg(ctx, "xfrozen.parse_body(which = resp_body) "
			    "called but no response body found");
			return (0);
		}
		else {
			r = ObjIterate(ctx->req->wrk, ctx->req->objcore,
			    cip, collect_iter_f, 0);
		}
	}
	else
		WRONG("which in parse_body");

	if (r != 0 && cip->len > cip->max) {
		errmsg(ctx, "xfrozen.parse_body(maxbytes = %zu) "
		    "exceeded", max);
		AZ(cip->heap);
		return (0);
	}
	if (r != 0) {
		errmsg(ctx, "xfrozen.parse_body() collect failed %d", r);
		free(cip->heap);
		return (0);
	}
	AZ(r);
	ret = parse(ctx, vfpaa, cip->ptr, cip->len);
	if (ret == 0) {
		free(cip->heap);
		return (ret);
	}
	if (cip->heap != NULL) {
		priv_task = VRT_priv_task(ctx, cip->heap);
		if (priv_task == NULL) {
			errmsg(ctx, "xfrozen.parse_body() out of workspace");
			free(cip->heap);
			return (0);
		}
		priv_task->priv = cip->heap;
		priv_task->methods = collect_priv_methods;
	}

	return (ret);
}

static const enum type_e json_type_2_type_e[JSON_TYPES_CNT] = {
	[JSON_TYPE_INVALID] = _TYPE_E_INVALID,
	[JSON_TYPE_STRING] = type_STRING,
	[JSON_TYPE_NUMBER] = type_NUMBER,
	[JSON_TYPE_TRUE] = type_BOOL,
	[JSON_TYPE_FALSE] = type_BOOL,
	[JSON_TYPE_NULL] = _TYPE_E_INVALID,
	[JSON_TYPE_OBJECT_START] = _TYPE_E_INVALID,
	[JSON_TYPE_OBJECT_END] = type_OBJECT,
	[JSON_TYPE_ARRAY_START] = _TYPE_E_INVALID,
	[JSON_TYPE_ARRAY_END] = type_ARRAY
};

static void
vfex_cb(void *callback_data, const char *name,
   size_t name_len, const char *path, const struct json_token *token)
{
	struct vmod_frozen_task *vfta;
	struct vmod_frozen_expect *vfex;
	enum type_e type;
	unsigned i;

	CAST_OBJ_NOTNULL(vfta, callback_data,
	    VMOD_FROZEN_TASK_MAGIC);

	(void) name;
	(void) name_len;

	if (vfta->n_expect == vfta->n_seen)
		return;

	AN(token);
	assert(token->type != JSON_TYPE_INVALID);
	if (token->type == JSON_TYPE_OBJECT_START ||
	    token->type == JSON_TYPE_ARRAY_START)
		return;

	assert(vfta->n_expect > vfta->n_seen);

	/* vfta->first marks the first expect not yet valid */
	for (i = vfta->first; i < vfta->n_expect; i++) {
		vfex = &vfta->vals[i];

		if (vfex->valid)
			continue;

		if (token->type == JSON_TYPE_NULL &&
		    vfex->nullok == 0)
			continue;

		type = json_type_2_type_e[token->type];

		if (vfex->type != type_ANY &&
		    vfex->type != type)
			continue;

		if (strcmp(vfex->path, path))
			continue;

		vfex->valid = 1;
		vfex->val = token->ptr;
		vfex->len = token->len;
		if (token->type == JSON_TYPE_NULL)
			vfex->null = 1;
		else
			vfex->type = type;

		vfta->n_seen++;

		/* advance vfta->first only if this is the first vfex */
		if (i != vfta->first)
			return;

		while (++i < vfta->n_expect) {
			vfex = &vfta->vals[i];
			if (vfex->valid == 0) {
				vfta->first = i;
				return;
			}
		}
		vfta->first = i;
		assert (vfta->n_seen == vfta->n_expect);
		return;
	}
}

VCL_STRING
vmod_parser_extract(VRT_CTX,
    struct vmod_frozen_parser *vfpaa, VCL_STRING path, VCL_STRING r_null,
    VCL_STRING r_undef)
{
	const struct vmod_frozen_parser *vfpa = vfpaa;
	struct vmod_frozen_task *vfta;
	struct vmod_frozen_expect *vfex;
	struct vmod_priv *task;
	unsigned i, n;

	CHECK_OBJ_NOTNULL(ctx, VRT_CTX_MAGIC);
	CHECK_OBJ_NOTNULL(vfpa, VMOD_FROZEN_PARSER_MAGIC);

	task = VRT_priv_task(ctx, vfpa);

	if (task == NULL) {
		VRT_fail(ctx, "no priv_task");
		return (r_undef);
	}

	if (task->priv == NULL) {
		VRT_fail(ctx, "no state from .parse()");
		return (r_undef);
	}

	CAST_OBJ_NOTNULL(vfta, task->priv,
	    VMOD_FROZEN_TASK_MAGIC);

	if (vfta->n_seen == 0)
		return (r_undef);

	for (n = 0; n < vfta->n_expect; n++) {
		i = (vfta->first + n) % vfta->n_expect;

		vfex = &vfta->vals[i];

		if (strcmp(vfex->path, path))
			continue;

		if (vfex->valid == 0)
			return (r_undef);

		if (vfex->null == 1)
			return (r_null);

		vfta->first = i;

		if (vfex->val == NULL || vfex->len == 0)
			return (r_undef);

		return (WS_Printf(ctx->ws, "%.*s", vfex->len, vfex->val));
	}

	return (r_undef);
}

VCL_STRING vmod_parser_type(VRT_CTX,
    struct vmod_frozen_parser *vfpaa, VCL_STRING path)
{
	const struct vmod_frozen_parser *vfpa = vfpaa;
	struct vmod_frozen_task *vfta;
	struct vmod_frozen_expect *vfex;
	struct vmod_priv *task;
	unsigned i, n;

	CHECK_OBJ_NOTNULL(ctx, VRT_CTX_MAGIC);
	CHECK_OBJ_NOTNULL(vfpa, VMOD_FROZEN_PARSER_MAGIC);

	task = VRT_priv_task(ctx, vfpa);

	if (task == NULL) {
		VRT_fail(ctx, "no priv_task");
		return (NULL);
	}

	if (task->priv == NULL) {
		VRT_fail(ctx, "no state from .match()");
		return (NULL);
	}

	CAST_OBJ_NOTNULL(vfta, task->priv,
	    VMOD_FROZEN_TASK_MAGIC);

	if (vfta->n_seen == 0)
		return (NULL);

	for (n = 0; n < vfta->n_expect; n++) {
		i = (vfta->first + n) % vfta->n_expect;
		vfex = &vfta->vals[i];

		if (strcmp(vfex->path, path))
			continue;

		if (vfex->valid == 0)
			return (NULL);

		if (vfex->null == 1)
			return (NULL);

		assert(vfex->type != type_ANY);

		return (type_s[vfex->type]);
	}

	return (NULL);
}
