# Copyright 2020 UPLEX Nils Goroll Systemoptimierung
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions
# are met:
# 1. Redistributions of source code must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
# 2. Redistributions in binary form must reproduce the above copyright
#    notice, this list of conditions and the following disclaimer in the
#    documentation and/or other materials provided with the distribution.
#
# THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED.  IN NO EVENT SHALL AUTHOR OR CONTRIBUTORS BE LIABLE
# FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
# DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
# OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
# HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
# LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
# OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
# SUCH DAMAGE.
#
# Author: Nils Goroll <nils.goroll@uplex.de>

vcl 4.1;

import crypto;

sub vcl_init {
	########################################
	# kid test
	new jwt_key_test = crypto.key();
	jwt_key_test.pem_pubkey({"-----BEGIN PUBLIC KEY-----
MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAuKnGOHLaY1aoAJJwegD/
w3v3Kk4DDhLmpH92CGJoB2qJo71cTLOFAnt2puAGCP7KYOyqhIuNo37ajb+jEGj7
GpqALW0OrptgNpFj2R/YH6xvQYdS98yjyC8qytTB8XhbsRJcmucn5DgmsCwXL5jQ
hS6AVjS8Lxr6zSE55Qhm5BgwN1AwT9V/byJxAtUiIel7knGwWd7tMHm7O3ZuT5E0
PRXJoJJqHPzcDp8zmdapNypmuzsgsBQoKaQBIv/wEm5UtpcZOFGT7yTVErI8H7A/
KgekUOFA5rJ6TVGWIe4IxiQIYsLQLEXt5vxnSvAospKjiOsjl0UB41mnDOkpXvee
rQIDAQAB
-----END PUBLIC KEY-----
"});
	new jwt_verify_test = crypto.verifier(sha256, key=jwt_key_test.use());
}

sub recv_jwt_verify_rsa {
	if (jwt_hdr.extract(".kid") == "test") {
		jwt_verify_test.reset();
		if (jwt_verify_test.update(jwt_subj.get()) &&
		    jwt_verify_test.valid(blob.decode(BASE64URLNOPAD,
				encoded=jwt_sig.get()))) {
			return;
		}
		return (synth(400, "jwt: bad sig kid test"));
	}
	return (synth(400, "jwt: unknown kid"));
}
