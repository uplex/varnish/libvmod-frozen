..
.. NB:  This file is machine generated, DO NOT EDIT!
..
.. Edit vmod.vcc and run make instead
..

.. role:: ref(emphasis)

.. _vmod_frozen(3):

===========
vmod_frozen
===========

---------------------
Varnish frozen Module
---------------------

:Manual section: 3

SYNOPSIS
========


::

   import frozen [from "path"] ;
   
   new xparser = parser(INT depth)
  
      VOID xparser.expect(STRING path, ENUM type, BOOL null, BOOL required)
  
      BOOL xparser.parse(STRING)
  
      STRING xparser.extract(STRING path, STRING null, STRING undef)
  
      STRING xparser.type(STRING path)
  


DESCRIPTION
===========

This vmod makes available to VCL the _frozen_ JSON parser with low
overhead: By specifying a set of expected JSON paths, a callback to
the parser is used to track only paths of interest, which can then be
extracted.

Example
    ::

	import frozen;

	sub vcl_init {
	    new jwt_hdr = frozen.parser();
	    jwt_hdr.expect(".alg", type=STRING, null=false, required=true);
	    jwt_hdr.expect(".typ", type=STRING, null=false, required=true);

	    new jwt_payload = frozen.parser();
	    jwt_payload.expect(".attr");

	}

	sub vcl_recv {
	    if (! jwt_hdr.parse(...) ||
		jwt_hdr.extract(".alg") != "RS256" ||
		jwt_hdr.extract(".typ") != "JWT") {
		return (synth(400, "unsupported jwt alg/typ"));
	    }

	    if (! jwt_payload.parse(...)) {
		return (synth(400, "parse error"));
	    }

	    set req.http.something =
		jwt_payload.extract(".attr",
				    null = "<null>", undef = "<undef>");

	    # ...
	}

INSTALL
=======

Building from source
--------------------

This VMOD is using the standard build procedure, but includes the
`frozen` parser as a submodule, so the source should be cloned using::

	git clone --recurse-submodules $URL

Quick start
-----------

This sequence should be enough in typical setups:

1. ``./bootstrap``
2. ``make``
3. ``make check`` (regression tests)
4. ``make install`` (may require root: sudo make install)

Alternative configs
-------------------

If you have installed Varnish to a non-standard directory, call
``autogen.sh`` and ``configure`` with ``PKG_CONFIG_PATH`` pointing to
the appropriate path. For example, when varnishd configure was called
with ``--prefix=$PREFIX``, use::

  PKG_CONFIG_PATH=${PREFIX}/lib/pkgconfig
  ACLOCAL_PATH=${PREFIX}/share/aclocal
  export PKG_CONFIG_PATH ACLOCAL_PATH

CONTENTS
========

* :ref:`obj_parser`
* :ref:`func_parser.expect`
* :ref:`func_parser.extract`
* :ref:`func_parser.parse`
* :ref:`func_parser.type`


.. _obj_parser:

new xparser = parser(INT depth=10)
----------------------------------

Instiantiate a JSON parser object.

The `depth` argument specifies the maximum recursion depth. This
should be set to the lowest possible value based on the expected input
to avoid stack overflows.

Without additional configuration using the `.expect()` method, a JSON
parser obect will just check the syntactic validity of JSON input.

.. _func_parser.expect:

parser.expect(...)
------------------

::

      VOID xparser.expect(
            STRING path,
            ENUM {ANY, STRING, NUMBER, BOOL, OBJECT, ARRAY} type=ANY,
            BOOL null=1,
            BOOL required=0
      )

Prepare a parser object for extration of a value at `path`, which is
constructed as follows:

* the root element has the empty path ``""``
* for objects, ``.`` (dot) is appended to the path
* for object keys, the key name is appended to the path
* for arrays, [ELEMENT_INDEX] is appended for each element

The `type` argument may be use to restrict matches to a particular
JSON type.

The `null` argument specifies if ``null`` is to be accepted as a valid
value. For ``type = ANY``, `null` is _not_ implied.

The `required` argument speifies if presence of this value is required
for successful `.match()`

This method may only be called from ``vcl_init {}``.

For best efficiency, call `.expect` according to the most likely order
of elements in JSON input to be parsed.

Errors will cause a VCL failure.

=== path examples ===

Consider this json string::

	{ "foo": 123, "bar": [ 1, 2, { "baz": true } ] }

The following examples are valid paths (values given for clarity)

* ``".foo" == "123"``
* ``".bar[0]" == "1"``
* ``".bar[2].baz" == "true"``


.. _func_parser.parse:

BOOL xparser.parse(STRING)
--------------------------

Parse the given string with the parser object.

The method will return `true`, if

    * The string represents valid JSON
    * The recursion depth is not exceeded
    * All expected paths with the `required` argument set to `true`
      have been found.

For expected paths, the first match is recorded.

Details on parse errors are logged as ``VCL_Error``


.. _func_parser.extract:

parser.extract(...)
-------------------

::

      STRING xparser.extract(
            STRING path,
            STRING null="",
            STRING undef=""
      )

After a successful `.parse()`, extract the given path, which must have
been declared in ``vcl_init {}`` using `.expect()`.

For JSON ``null`` values, the `null` argument is returned.

For paths which are not found and not defined as required, the `undef`
argument is returned.


.. _func_parser.type:

STRING xparser.type(STRING path)
--------------------------------

Return the JSON type of an extracted path or NULL otherwise.

This is particularly relevant for paths expected to be of type ANY if
strings need to be differentiated from other types.

SEE ALSO
========vcl\(7),varnishd\(1)





COPYRIGHT
=========

::

  Copyright 2018 UPLEX Nils Goroll Systemoptimierung
  All rights reserved
 
  Author: Nils Goroll <nils.goroll@uplex.de>
 
  See LICENSE
 
